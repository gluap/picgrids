import unittest
import particles2grid
import numpy
class TestParticleObject(unittest.TestCase):
    def test_creation(self):
        testarray=numpy.random.random((100000,2))
        particles=particles2grid.particles(testarray)
        self.assertEqual( particles.particleNumber,100000, "particle class treats particle number wrongly")
        self.assertEqual( particles.particleCharge,1., "Default charge should be 1")
        self.assertEqual( particles.coordinateNames["x"],0., "Default first coordinate is x coordinate")
        self.assertEqual( particles.coordinateNames["y"],1., "Default second coordinate is y coordinate")
class TestGridSumming(unittest.TestCase):
    def test_grid_initialization_spacing1(self):
        testGrid=particles2grid.Cgrid1D(left=0,right=1,nBins=2)
        self.assertEqual(testGrid.gridSpacing,.5,"gridspacing for two bins on [0,1] should be .5 but is %f" % testGrid.gridSpacing)
        self.assertTrue(numpy.allclose(testGrid.bins,[.25,.75]))
    def test_grid_initialization_spacing2(self):
        testGrid=particles2grid.Cgrid1D(left=0,right=150,nBins=2)
        self.assertEqual(testGrid.gridSpacing,150./2.,"gridspacing for two bins on [0,1] should be .5 but is %f" % testGrid.gridSpacing)
        self.assertTrue(numpy.allclose(testGrid.bins,[150/4.,3*150./4.]))
    def test_grid_binning1(self):
        testGrid=particles2grid.Cgrid1D(left=0,right=1,nBins=2)
        testGrid.points2Grid(numpy.array([[0.25,1.]],dtype="float"),direction=0)
        self.assertTrue(numpy.allclose(testGrid.density,[2.,0.]), "testing with one gridpoint error")
        testGrid.points2Grid(numpy.array([[0.25,1.]],dtype="float"),direction=0)
        self.assertTrue(numpy.allclose(testGrid.density,[2.,0.]), "grid does not auto-reset for new binning")
        testGrid.points2Grid(numpy.array([[0,1.]],dtype="float"),direction=0)
        self.assertTrue(numpy.allclose(testGrid.density,[1.,1.]), "grid is not circular")
    def test_grid_binning2(self):
        testGrid=particles2grid.Cgrid1D(left=0,right=1,nBins=2)
        testGrid.points2Grid(numpy.array([[0.25,1.]],dtype="float"),direction=1)
        self.assertTrue(numpy.allclose(testGrid.density,[1.,1]), "second grid coordinate does not give expected results")
        testGrid.points2Grid(numpy.array([[0.25,1.]],dtype="float"),pointMass=.5,direction=1)
        self.assertTrue(numpy.allclose(testGrid.density,[.5,.5]), "density works for one particle")
    def test_many_particles(self):
        testGrid=particles2grid.Cgrid1D(left=0,right=1,nBins=2)
        testGrid.points2Grid(numpy.random.random((100000,2)),direction=1)
        totalDensity= numpy.sum(testGrid.density)
        self.assertAlmostEqual(totalDensity,100000./0.5,msg= "sum of density for 100k particles with weight 1 and binwidth 0.5 should be 200k but is %d" % totalDensity)
    def test_many_particles_density(self):
        testGrid=particles2grid.Cgrid1D(left=0,right=1,nBins=2)
        testGrid.points2Grid(numpy.random.random((100000,2)),direction=1,pointMass=.137)
  #      print numpy.sum(testGrid.density)
        self.assertAlmostEqual(numpy.sum(testGrid.density),0.137*100000./0.5,msg= "total density on grid should be equal particle number for particleCharge=1.")
    def test_grid2Points(self):
        testGrid=particles2grid.Cgrid1D(left=0,right=1,nBins=2)
        testGrid.density=numpy.array([1.0,0.0])
        testVector=numpy.array([[.25,0],[.75,0],[.3+.25,0]])
        testGrid.grid2Points(testVector)
        self.assertTrue(numpy.allclose(testVector,[[.25,1.],[.75,0.],[.3+.25,0.4]]),msg="wrong numbers from points2grid method")
    def test_grid2Points_charge(self):
        testGrid=particles2grid.Cgrid1D(left=0,right=1,nBins=2)
        testGrid.density=numpy.array([1.0,0.0])
        testVector=numpy.array([[0.25,0],[.5+.25,0],[.3+.25,0]])
        testGrid.grid2Points(testVector,pointMass=.5)
        self.assertTrue(numpy.allclose(testVector,[[0.+.25,.5],[.5+.25,0.],[.3+.25,0.2]]),msg="wrong numbers from points2grid method when mass !=1")
    def test_grid2Points_charge2(self):
        bns=30000
        testGrid=particles2grid.Cgrid1D(left=0,right=15,nBins=bns)
        sine=1.*numpy.sin(testGrid.bins,dtype=numpy.float64)
        testGrid.density=sine
    #    print testGrid.density
     #   print testGrid.bins
        testVector=numpy.array([testGrid.bins+.1e-14,numpy.zeros(bns)]).T.copy()
     #   print testVector.shape
        testGrid.grid2Points(testVector,pointMass=1)
  #      print testVector[0:100,1]
  #      print ((-testVector+numpy.array([testGrid.bins,sine]).T.copy())[0:,1])[0:100]
  #      print ((-testVector+numpy.array([testGrid.bins,sine]).T.copy())[0:,1])[59]#

  #      for i in numpy.arange(0,bns):
  #          tst= ((testVector)[0:,1])[i]-numpy.array([testGrid.bins,sine]).T.copy()[i,1]
   #         if abs(tst)>1e-10:
    #            print i
    #        if abs((-testVector+numpy.array([testGrid.bins,sine]).T.copy())[i,1])>1e-10:
     #           print "%d: %e,%e,%e %e sine: %e" % (i,tst,(-testVector+numpy.array([testGrid.bins,sine]).T)[i,1],((testVector)[0:,1])[i],numpy.array([testGrid.bins,sine])[1,i],sine[i])     

      #  print "%e" % max(abs(testVector-numpy.array([testGrid.bins+sine,sine]).T)[0:,1])
       # print "total: %e" % sum(abs(testVector-numpy.array([testGrid.bins+sine,sine]).T)[0:,1])
        #print testVector
        self.assertTrue(numpy.allclose(testVector[0:,1],numpy.array([testGrid.bins,numpy.sin(testGrid.bins+1e-14)]).T.copy()[0:,1],atol=.001),msg="wrong numbers from points2grid method when sine !=1")

class testGridParticles(unittest.TestCase):
    def test_compare_particles_points(self):
        testCoordinates=numpy.random.random((100000,2))
        particles=particles2grid.particles(testCoordinates)
        testGridParticles=particles2grid.Cgrid1D(left=0,right=1,nBins=100)
        testGridParticles.particles2Grid(particles)
        testGridNormal=particles2grid.Cgrid1D(left=0,right=1,nBins=100)
        testGridNormal.points2Grid(testCoordinates)
        self.assertTrue(numpy.allclose(testGridNormal.density,testGridParticles.density),"Binning particle object and pure coordinates gives different result")
#        testGrid.points2Grid(numpy.array([[.5,100]]))
    def test_particles_points_densities(self):
        testCoordinates=numpy.random.random((100000,2))
        particles=particles2grid.particles(testCoordinates,particleCharge=10.379)
        testGridParticles=particles2grid.Cgrid1D(left=0,right=1,nBins=100)
        testGridParticles.particles2Grid(particles)
        testGridNormal=particles2grid.Cgrid1D(left=0,right=1,nBins=100)
        testGridNormal.points2Grid(testCoordinates,pointMass=10.379)
        self.assertTrue(numpy.allclose(testGridNormal.density,testGridParticles.density),"Binning particle object and pure coordinates gives different result")
class testGridPoints2D(unittest.TestCase):
    def test_grid_2D(self):
        testCoordinates=numpy.array([[.5,.5]])
        grid2D=particles2grid.Cgrid2D(xmin=0,ymin=0,xmax=1,ymax=1,nBinsX=2,nBinsY=2)
        grid2D.points2Grid(testCoordinates)
        self.assertTrue(numpy.allclose(grid2D.density,numpy.array([[.25,.25],[.25,.25]])*1/grid2D.gridSpacingX/grid2D.gridSpacingY),"particle grid not centered or or wrong binning")
    def test_grid_2D_strange(self):
        testCoordinates=numpy.array([[0.,0]])
        grid2D=particles2grid.Cgrid2D(xmin=0,ymin=0,xmax=1,ymax=1,nBinsX=2,nBinsY=2)
        grid2D.points2Grid(testCoordinates)
        self.assertTrue(numpy.allclose(grid2D.density,numpy.array([[.25,.25],[.25,.25]])*1/grid2D.gridSpacingX/grid2D.gridSpacingY),"particle grid not centered or wrong binning")
    def test_grid_2D_sum(self):
        testCoordinates=numpy.random.random((10000,2))
        grid2D=particles2grid.Cgrid2D(xmin=0,ymin=0,xmax=1,ymax=1,nBinsX=100,nBinsY=100)
        grid2D.points2Grid(testCoordinates)
        self.assertAlmostEqual(numpy.sum(grid2D.density), 10000./grid2D.gridSpacingX/grid2D.gridSpacingY,6  ,msg="density sum not equal particle number %f %f" % (numpy.sum(grid2D.density), 10000./grid2D.gridSpacingX/grid2D.gridSpacingY))
    def test_grid_2D_sumMass(self):
        testCoordinates=numpy.random.random((10000,2))
        grid2D=particles2grid.Cgrid2D(xmin=0,ymin=0,xmax=1,ymax=1,nBinsX=100,nBinsY=100)
        grid2D.points2Grid(testCoordinates,pointMass=.5)
        self.assertAlmostEqual(numpy.sum(grid2D.density), 0.5*10000/grid2D.gridSpacingX/grid2D.gridSpacingY,6  ,msg="density sum not equal particle number*pointMass %f != %f " % (numpy.sum(grid2D.density),10000/grid2D.gridSpacingX/grid2D.gridSpacingY))
class testGridParticles2D(unittest.TestCase):
    def test_grid_2D(self):
        testCoordinates=numpy.array([[.5,.5]])
        testParticles=particles2grid.particles(testCoordinates)
        grid2D=particles2grid.Cgrid2D(xmin=0,ymin=0,xmax=1,ymax=1,nBinsX=2,nBinsY=2)
        grid2D.particles2Grid(testParticles)
        self.assertTrue(numpy.allclose(grid2D.density,numpy.array([[.25,.25],[.25,.25]])/grid2D.gridSpacingX/grid2D.gridSpacingY),"particle grid not centered or wrong binning")
    def test_grid_2D_sum(self):
        testCoordinates=numpy.random.random((10000,2))
        testParticles=particles2grid.particles(testCoordinates)
        grid2D=particles2grid.Cgrid2D(xmin=0,ymin=0,xmax=1,ymax=1,nBinsX=2,nBinsY=2)
        grid2D.particles2Grid(testParticles)
        self.assertAlmostEqual(numpy.sum(grid2D.density), 10000/grid2D.gridSpacingX/grid2D.gridSpacingY,6 ,msg="density sum not equal particle number %f != %f" % (numpy.sum(grid2D.density),10000/grid2D.gridSpacingX/grid2D.gridSpacingY ))
    def test_grid_2D_sumMass(self):
        testCoordinates=numpy.random.random((10000,2))
        testParticles=particles2grid.particles(testCoordinates,particleCharge=.5)
        grid2D=particles2grid.Cgrid2D(xmin=0,ymin=0,xmax=1,ymax=1,nBinsX=2,nBinsY=2)
        grid2D.particles2Grid(testParticles)
        self.assertAlmostEqual(numpy.sum(grid2D.density),.5*10000/grid2D.gridSpacingX/grid2D.gridSpacingY,6  ,msg="density sum not equal particle number %f*particleCharge" % numpy.sum(grid2D.density))

if __name__ == '__main__':
    unittest.main()