# particles2grid

This python module offers a simple and fast method to project n-dimensional numpy arrays onto a 1D-Grid along one axis. I mostly wrote it to teach myself about improving speed in cython. The final cython version runs about 250 times faster than the pure python version.

Documentation can be found in the cython file `particles2grid.pyx`.

To understand better how the methods work you can also look at the unit tests (which can be executed via `test.py`).

To build the module run `python setup.py build_ext --inplace`
